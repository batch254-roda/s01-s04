package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @PostMapping(value="/posts")
    //"ResponseEntity" represent the whole HTTP response: status code, headers, and body.
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post) {

        // We can access the "postService" methods and pass the following arguments:
        //stringToken of the current session will ve retrieved from the request headers.
        //a "post" object will be instantiated upon receiving the request body, and this will follow the properties defined in the Post model.
        // note: the "key" name of the request from postman should be similar to the property names defined in the model.
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);

    }

    @GetMapping(value = "/posts")
    public ResponseEntity<Object> getPost(){
        postService.getPosts().forEach(post -> post.getUser().setPassword("Confidential"));
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/myPosts")
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getUserPosts(stringToken), HttpStatus.OK);
    }

    @PutMapping(value = "/posts/{postId}")
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postId, stringToken, post);
//        return new ResponseEntity<>(postService.updatePost(postId, stringToken, post), HttpStatus.OK);
    }

    @DeleteMapping(value = "/posts/{postId}")
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
//        return new ResponseEntity<>(postService.deletePost(postId, stringToken), HttpStatus.OK);
    }

}
