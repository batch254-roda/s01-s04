package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // create a post function
    @Override
    public void createPost(String stringToken, Post post) {
        // findByUsername to retrieve the user
        //Criteria for finding the user is from the jwtToken method getUsernameFromToken.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        // the title and content will come from the reqBody which is passed through the post identifier.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        // author retrieve from the token
        newPost.setUser(author);

        // the actual saving of post in our table.
        postRepository.save(newPost);
    }


    // getting all posts
    @Override
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public Iterable<Post> getUserPosts(String stringToken){
        ArrayList<Post> foundPosts = new ArrayList<>();
        Iterable<Post> userPosts = postRepository.findAll();
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        userPosts.forEach(post -> {
                        if(post.getUser().getUsername().equals(author.getUsername())){
                            foundPosts.add(post);
                        }
                    }
                );
        return foundPosts;
    }

    @Override
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity("Post Updated Successfully!", HttpStatus.OK);
        } else {
            return new ResponseEntity("You are not Authorized to edit this post!", HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeletion = postRepository.findById(id).get();

        String postAuthor = postForDeletion.getUser().getUsername();

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity("Post Deleted Successfully!", HttpStatus.OK);
        } else {
            return new ResponseEntity("You rare not Authorized to delete this post!", HttpStatus.UNAUTHORIZED);
        }
    }


}
